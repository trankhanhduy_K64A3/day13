<?php
    include 'connect.php';

    $faculty_list = array(""=>"", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
    $gender_list = ["Nữ", "Nam"];
    if(isset($_GET["search"]) && !empty($_GET["search"])){
        $searching = $conn -> query("select * from student where name like '%$key%' or address like '%$key%' ");
        $student_list = $searching -> fetchAll();
    }
    else{   
        $data = $conn->query("select * from student");
        $student_list = $data->fetchAll();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/table_sv.css">
</head>

<body>
    <div class="wrapper">
        <form method="post" enctype="multipart/form-data">
            <div class="input-box faculty-box">
                <label for="faculty" class="label-input">
                    Khoa
                </label>
                <select name="faculty" id="faculty" class="select-field">
                    <?php foreach ($faculty_list as $key => $value) { ?>
                        <option value=<?php echo $key ?>><?php echo $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="input-box name-box">
                <label for="" class="label-input">
                    Từ khoá
                </label>
                <input type="text" class="text-field" name="userName">
            </div>
            <div class="button-box">
                <button class="button-submit" type="submit">Tìm kiếm</button>
            </div>
        </form>
        <div class="input-box name-box" style=" padding: 10px;">
            <p style="float: left;">Số sinh viên tìm thấy: <?php echo sizeof($student_list) ?></p>
            <form action="register.php" style="float: right;" method="post">
                <div>
                        <button class="button-submit">Thêm</button>
                </div>
            </form>
        </div>
        <div class="list_student">
            <table style="width:100%">
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <?php
                foreach($student_list1 as $student) {
            ?>
            <tr>
                <td><?php echo $student['id'] ?></td>
                <td><?php echo $student['name'] ?></td>
                <td><?php echo  $faculty_list[$student['faculty']] ?></td>
                <td>
                    <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.8;color: white;">Xóa</button>
                    <button class="button_action" type="submit" formaction="" style="border: 1px solid #41719c;padding: 3px 10px;background-color: #4273b1;opacity: 0.8;color: white;">Sửa</button>
                </td>
            </tr>
            <?php
                }
            ?>
            </table>
        </div>
    </div>
</body>

</html>